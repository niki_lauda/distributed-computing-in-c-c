#ifndef __MATRIX_H_
#define __MATRIX_H_

#include <string>
#include <fstream>
#include <vector>

class Matrix{
  private:
    std::vector<std::vector<int>> _matrix;
    int _intendLevel;
  public:
    Matrix(int rows, int columns);
    Matrix(const std::vector<std::vector<int>> matrix);
    ~Matrix();
    void init();
    void print();
    void randomize(int range);
    void save(std::ofstream& out) const;
    void pretty();
    int rows();
    int columns();
    
    class Proxy {
      public:
        Proxy(std::vector<int>* vector) : _vector(vector) { }

        int& operator[](int index) {
            return _vector->at(index);
        }
      private:
        std::vector<int>* _vector;
    };
    
    Proxy operator [](const int& i);
    static std::vector<Matrix> parse(std::string src);
    static void generate(std::ofstream& out, int rows, int columns, int rand_range, int count, bool pretty);
};
#endif

