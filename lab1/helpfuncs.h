#ifndef __MATRIX1_H_
#define __MATRIX1_H_

#include <chrono>
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <string>
#include "matrix.h"

typedef std::chrono::high_resolution_clock chrono;

int average(Matrix matrix, int i, int j){
  int directions[8][2]  = {{-1,-1}, {-1,0}, {-1,1},  {0,1}, {1,1},  {1,0},  {1,-1},  {0, -1}};
  int sum = 0, failed = 0;
  for (int k = 0; k < 8; k++) {
    try{
      sum += matrix[i + directions[k][0]][j + directions[k][1]];
    }
    catch(const std::out_of_range& e){
      failed++;
    }
  }
  return sum/(8 - failed);
}

void replaceElems(Matrix& m, Matrix& res){
  Matrix retM(m.rows(), m.columns());
  for(int i = 0; i < m.rows(); i++){
    for(int j = 0; j < m.columns(); j++){
      res[i][j] = average(m, i, j);
    }
  }
}

void timer(std::string msg = ""){
  std::cout<< "\e[1;32m" << msg << "\e[0m";
  static bool launched = false;
  static chrono::time_point t1;
  static chrono::time_point t2;
  if(!launched){
    launched = !launched;
    t1 = chrono::now();
    return;
  }
  t2 = chrono::now();
  launched = !launched;
  std::cout << "\e[1;32mDuration: "
  << std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count()
  << " milliseconds \e[0m\n";
}
#endif
