#include <chrono>
#include "matrix.h"
#include "helpfuncs.h"
//g++ main.cpp matrix.h matrix.cpp -I/usr/include/boost -L/usr/lib/boost -lboost_filesystem -lboost_system -lboost_unit_test_framework -std=c++11

void program(){
  const std::string url = "output.txt";
  std::ofstream writer(url, std::ios::out);
  timer("Generating data...\n");
  Matrix::generate(writer, 10, 10, 1000, 10000, true);
  timer("Completed.");
  writer.close();
  std::vector<Matrix> matrixes = Matrix::parse("output.txt");
  std::vector<Matrix> resultMatrixes;
  timer("Performing algorithm replacing elements...\n");
  for(auto m : matrixes){
    Matrix res(m.rows(), m.columns());
    replaceElems(m, res);
    resultMatrixes.push_back(res);
  }
  timer("Completed.");
}

int main(int argc, char **argv){
  program();
  return 0;
}