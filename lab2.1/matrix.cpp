#include "matrix.h"
#include <iomanip>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/test/unit_test.hpp>

Matrix::Matrix(int rows, int columns){
  _matrix.resize(rows);
  for(int i = 0; i < _matrix.size(); i++){
    _matrix[i].resize(columns);
  }
}

Matrix::Matrix(std::vector<std::vector<int>> matrix){
  _matrix = matrix;
}

Matrix::~Matrix(){
  std::vector<std::vector<int>>().swap(_matrix);
}

void Matrix::pretty(){
  int max = std::accumulate(_matrix.begin(), _matrix.end(), _matrix[0][0],
      [](int max, const std::vector<int> &v){
         return std::max(max,*std::max_element(v.begin(), v.end()));});
  std::stringstream ss;
  ss << max;
  _intendLevel = ss.str().length() + 1;
}

void Matrix::randomize(int range){
  for(int i = 0; i < _matrix.size(); i++)
    for(int j = 0; j < _matrix[0].size(); j++)
      _matrix[i][j] = rand() % range + 1;
}

void Matrix::print(){
  for(int i = 0; i < _matrix.size(); i++){
    for(int j = 0; j < _matrix[0].size(); j++){
      std::cout << _matrix[i][j] << std::setw(_intendLevel);
    }
    std::cout << std::endl;
  }
}

void Matrix::save(std::ofstream& out) const{
  for(int i = 0; i < _matrix.size(); i++){
    for(int j = 0; j < _matrix[0].size(); j++){
      out << _matrix[i][j] << std::setw(_intendLevel);
    }
    out << std::endl;
  }
  out << std::endl;
}

std::vector<Matrix> Matrix::parse(std::string src){
  boost::filesystem::path file(src);
  boost::system::error_code ec;
  if(!boost::filesystem::exists(src,ec) || !boost::filesystem::is_regular_file(file, ec)){
    throw std::invalid_argument("No such directory");
  }
  std::vector<Matrix> matrixes;
  std::ifstream reader(src, std::ios::in);
  if(!reader.is_open())
    return matrixes;
  
  std::string line;
  std::vector<std::vector<int>> vectors;
  for(int i = 0; std::getline(reader, line); ){
    if(line.empty()){
      continue;
    }
    std::istringstream is( line );
    std::vector<int> values(std::istream_iterator<int>(is), (std::istream_iterator<int>()));
    vectors.push_back(values);
    
    if(i == values.size() - 1){
      Matrix matrix(vectors);
      matrixes.push_back(matrix);
      vectors.clear();
      i = 0;
      continue;
    }
    i++;
  }
  return matrixes;
}

void Matrix::generate(std::ofstream& out, int rows, int columns, int rand_range, int count, bool pretty){
  srand(time(NULL));
  Matrix matrix(rows, columns);
  for(int i = 0; i < count; i++){
    matrix.randomize(rand_range);
    if(pretty)
      matrix.pretty();
    matrix.save(out);
    out << std::endl;
  }
}

Matrix::Proxy Matrix::operator [](const int& index){
  return Proxy(&_matrix.at(index));
}

int Matrix::rows(){
  return _matrix.size();
}

int Matrix::columns(){
  return _matrix[0].size();
}

