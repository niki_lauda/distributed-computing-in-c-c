#include <chrono>
#include "matrix.h"
#include "helpfuncs.h"
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <limits.h>
//g++ main.cpp matrix.h matrix.cpp -I/usr/include/boost -L/usr/lib/boost -lboost_filesystem -lboost_system -lboost_unit_test_framework -std=c++11

const std::string url = "output.txt";

void generate(){
  const int matrixCount = 1000000;
  std::ofstream writer(url, std::ios::out);
  timer("Generating data...\n");
  Matrix::generate(writer, 10, 10, 10000, matrixCount, true);
  timer("Completed generation.");
  writer.close();
}

void action(Matrix m, std::ofstream& writer){
  Matrix res = Matrix(m.rows(), m.columns());
  replaceElems(m, res);
  res.pretty();
  res.save(writer);
}

int program(){
  timer("Parsing data from file...\n");
  std::vector<Matrix> matrixes = Matrix::parse(url);
  timer("Finished parsing.");
  std::vector<Matrix> resultMatrixes;
  
  std::ofstream writer("resultOutput.txt", std::ios::out);

  pid_t pid;
  int mypipe[2];
  if (pipe (mypipe)){
    perror ("Pipe failed.\n");
    writer.close();
    return EXIT_FAILURE;
  }
  
  timer("Performing algorithm replacing elements...\n");
  pid = fork ();
  if (pid < (pid_t) 0){
    perror ("Fork failed.\n");
    writer.close();
    return EXIT_FAILURE;
  }
  else if (pid == (pid_t) 0){
    close (mypipe[0]);
    int index = 0;
    for(int i = 0; i < matrixes.size() - 1; i+= 2){
      action(matrixes[i], writer);
      index = i + 1;
      if (write (mypipe[1], &index, sizeof(index)) != sizeof(index)) {
         perror ("write");
         writer.close();
         exit (EXIT_FAILURE);
      }
    }
  }
  else{
    close (mypipe[1]);
    int index = 0;
    while(read (mypipe[0], &index, sizeof(index))){
      action(matrixes[index], writer);
    }
    
    if ((waitpid (pid, NULL, 0)) < 0) {
         perror ("waitpid");
         writer.close();
         exit (EXIT_FAILURE);
      }
    timer("Algorithm completed...\n");
    writer.close();
    return EXIT_SUCCESS;
  }

}

int main(int argc, char **argv){
  program();
  return 0;
}