# **LAB1. Var. 09** #

Program Logic
```
#!
g++ main.cpp matrix.h helpfuncs.h matrix.cpp -I/usr/include/boost -L/usr/lib/boost -lboost_filesystem -lboost_system -lboost_unit_test_framework -std=c++11
```

Boost Tests
```
#!

g++ tests.cpp matrix.h  matrix.cpp helpfuncs.h -I/usr/include/boost -L/usr/lib/boost -lboost_filesystem -lboost_system -lboost_unit_test_framework -std=c++11
```

# **LAB2.1 Var. 09** #

Using Pipes as a mean of IPC

An EXAMPLE OF IPC through pipes: Forked child process is processing an i-Matrix from the Matrix-vector and passing i+1 index to the parent process, that is processing Matrix with index i+1 in Matrix-vector;

# **LAB2.2 Var. 09** #

Using Message Queues as a mean of IPC

An EXAMPLE OF IPC through using Message Queues : the parent process is processing an i + 1 Matrix from the vector of matrixes and passing i+1 index to the forked child process, that is processing Matrix with index i+1 from the vector of matrixes;
