#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "matrix.h"
#include "helpfuncs.h"
//g++ tests.cpp matrix.h  matrix.cpp helpfuncs.h -I/usr/include/boost -L/usr/lib/boost -lboost_filesystem -lboost_system -lboost_unit_test_framework -std=c++11

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(calcAverageAround){
  Matrix m(3, 3);
  m.randomize(10);
  
  //average around elem with indexes [1] [1]
  int sum = m[0][0] + m[0][1] + m[0][2] + m[1][0] + m[1][2] + m[2][0] + m[2][1]+m[2][2];
  int expected = sum / 8;
  
  BOOST_CHECK(expected == average(m, 1, 1));
  expected++;
  BOOST_CHECK(expected != average(m, 1, 1));
}

BOOST_AUTO_TEST_CASE(correctReplace){
  Matrix m(3, 3);
  m.randomize(10);
  Matrix res(3, 3);
  
  BOOST_CHECK(res[0][0] != average(m, 0, 0));
  replaceElems(m, res);
  BOOST_CHECK(res[0][0] == average(m, 0, 0));
}

BOOST_AUTO_TEST_SUITE_END()