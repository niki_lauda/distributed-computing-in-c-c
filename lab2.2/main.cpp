#include "matrix.h"
#include "helpfuncs.h"
#include <stdio.h>
#include <sys/msg.h>
#include <sys/wait.h>
//g++ main.cpp matrix.h matrix.cpp -I/usr/include/boost -L/usr/lib/boost -lboost_filesystem -lboost_system -lboost_unit_test_framework -std=c++11

typedef struct mymsgbuf{
  long mtype;
  int matrixIndex;
} mess_t;

const std::string url = "output.txt";

void generate(){
  const int matrixCount = 1000000;
  std::ofstream writer(url, std::ios::out);
  timer("Generating data...\n");
  Matrix::generate(writer, 10, 10, 10000, matrixCount, true);
  timer("Completed generation.");
  writer.close();
}

void action(Matrix m, std::ofstream& writer){
  Matrix res = Matrix(m.rows(), m.columns());
  replaceElems(m, res);
  res.pretty();
  res.save(writer);
}

int program(){
  generate();
  timer("Parsing data from file...\n");
  std::vector<Matrix> matrixes = Matrix::parse(url);
  timer("Finished parsing.");
  std::vector<Matrix> resultMatrixes;
  std::ofstream writer("resultOutput.txt", std::ios::out);
  
  key_t msgkey;
  pid_t pid;
  mess_t buf;
  buf.mtype = 1;
  msgkey = ftok(".",'m');
  int length = sizeof(mess_t) - sizeof(long);
  int qid = msgget(msgkey, IPC_CREAT | 0660);
  
  timer("Performing algorithm replacing elements...\n");
  pid = fork();
  if(!pid){
    while( buf.mtype != 2){
      msgrcv(qid, &buf, length, 0, 0);
      action(matrixes[buf.matrixIndex],writer);
    }
    return 0;
  }

  for(int i = 0; i < matrixes.size() - 1; i+=2){
      buf.matrixIndex = i + 1;
      msgsnd(qid, &buf, length, 0);
      action(matrixes[i], writer);
  }
  
  buf.mtype = 2;
  msgsnd(qid, &buf, length, 0);
  
  if ((waitpid (pid, NULL, 0)) < 0) {
    perror ("waitpid");
    writer.close();
    exit (EXIT_FAILURE);
  }
  msgctl(qid, IPC_RMID, 0);
  timer("Algorithm completed...\n");
  writer.close();
  return 0;
}

int main(int argc, char **argv){
  program();
  return 0;
}